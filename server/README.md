# Server-side architecture

There is no standard de-facto in building server-side apps using Node.js, unlike in front-end we have no ready to use libs like Redux or MobX which is proposing project-structure basics, and unlike other server-side platforms, we have no famous widely used complex frameworks like Django or Rails. But that is not so bad, in other languages we also have demands for tiny frameworks like Slim, Flask or Mojolicious, so it has own advantages.

Most of the Node.js projects are build from scratch using micro-frameworks and libraries, and in our case it's:

* [Express](https://expressjs.com/) - Micro-web framework which only cares about HTTP things
* [Sequelize](https://sequelize.org/) - Object-Relational Mapping, base for our model
* [LIVR](https://github.com/koorchik/js-validator-livr) - Flexible and universal validator

Using these libraries we building layers on our project.

## Layers sequence diagram

```
Note over Client: Creates HTTP request
Client->Controller: HTTP request

Note over Service: Have no idea about \nupper layers (Controller)
Note over Model: Have no idea about upper \nlayers (Controller, Service)

Note over Controller: Extracts data from request and \n pass to appropriate service
Controller->Service: JS object

Note over Service: Do some validation \n Execute any business-logic
Note over Service: May query Model for any data
Service-->Model:
Note over Model: Knows how to extract requested \ndata from cache or db
Model-->Service:

Note over Service: May interact with Jira instance
Service-->Jira instance:
Jira instance-->Service:

Service->Controller: JS object (often in Promise)
Note over Controller: Knows how to send this\n data in response

Controller->Client: HTTP response
```

[See rendered sequence diagram here](https://goo.gl/f2A8s3)

### Routes (Controller) layer

Top application layer. This layer responsibility is managing incoming events and provide abstraction from transports of this events.
It uses Express for dealing with HTTP things (like requests, responses, statuses).

### Services layer

Middle application layer. Implementation of [same-named pattern](https://martinfowler.com/eaaCatalog/serviceLayer.html).

* Accepts plain objects as input
* Runs input validation using `LIVR`
* In this particular app it also implements most of business logic, so it is also a "Domain Model" from diagram above
* Knows about model and using it for storing data
* Don't knows about a controller and not managing HTTP things
* Returns plain data (usually inside a `Promise`)

### Model layer

Bottom layer. In this app it's mostly a simple store, build on top of `Sequalize` ORM classes.
Provides an abstraction from SQL for use it in Services layer.

## Project functionality overall and used solutions

### Storage and cache

We use postgres and memcached for storing our data and cache. Model are build using the Sequalize ORM classes so we don't write SQL manually.

Our cache is a simple key:value store mainly for frequently used model objects. Key format is `model:<Class>:<ObjectId>`. We use [sequelize-transparent-cache](https://www.npmjs.com/package/sequelize-transparent-cache) for caching. This adapter adds `.cache()` method for each Sequalize instance and object. This method returns an object with wrapped subset of standard sequalize methods which is syncing the cache on write and query cache first on read. This wrapped methods returns regular sequalize objects so using the cache is easy as cake!

```javascript
// Load instance by id
model.Instance.findByPk(instanceId)

// The same but first query the cache. If instance was found in cache - return it; if not - query db and update cache
model.Instance.cache().findByPk(instanceId)
```

### Sessions and authentication

We use many different types of authentications. Here them all explained:

* Incoming events (web-hooks) from Jira: handled by our own class `lib/routes/sessions/Jira`.
* API calls. We use our own tokens to pass session information from client to server. It's including Jira user information. We use `window.AP.context.getToken()` to get token for passing authentication at all API endpoints.
* Admin API calls. Regular `ASAP` check implemented in `lib/admin/Sessions`.

### Errors

**General error handling principals**.
Since the project is split into layers, we can apply error handling best practices.

* Each layer throws its own errors (class like `lib/services/Error` or sometimes simply structure which has enough data to identify this structure as some layer’s error)
* Higher layer knows about errors of lower layer and can suppress it or rethrow as own error
* If higher layer get an error different from what we expect - it just rethrow this error without modification
* At the highest top layer, we have the final `catch` for all unhandled errors by previous layers. We can log this errors in a single place, in this project it’s `lib/routes/Route`

**Errors and HTTP responses**.
All well-known errors (like session expired) will be returned in JSON format with conditional fields.
If it's an application error - response will be the same, we just use different code `UNKNOWN_ERROR`. We are **never** returning a stack traces, that's not a best practice. Logs much more better place for stack traces.

## Tests

This project contains some amount of tests in the appropriate folder. **These are NOT unit tests**. This is an integration and running tests that lead to very close to production work behavior since we use real postgres, memcached and sqs resources spawned containers only external API’s responses for each test and mock in docker.

To run tests, you need to have Postgres, Memcached up and running `docker-compose up`.

Then you can simply run npm test for run all the tests.

### Running tests using Jest CLI

* To run all test use `npm test`
* To get code coverage report run `npm test -- --coverage`
* To run single test use `npm test -- filename`.
* Run "smart" watch mode with `npm test -- --watch`
