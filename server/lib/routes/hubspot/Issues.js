const { groupBy, upperFirst } = require('lodash')
const Route = require('../Route')

const ISSUES_LIMIT = 5

module.exports = class extends Route {
  async list (req, res) {
    const recordType = req.query.associatedObjectType.toLowerCase()

    const issues = await this.initService('issues.List', {
      recordId: req.query.associatedObjectId,
      recordType: upperFirst(recordType),
    }).run(req)

    const connections = await this.model.Connection.findAll({
      include: {
        model: this.model.Account,
        where: { externalId: req.context.externalId },
      },
    })

    const connectedInstancesLength = Object
      .keys(groupBy(connections, 'instanceId'))
      .length

    if (issues.length > ISSUES_LIMIT && connectedInstancesLength === 1) {
      // show all items link only when 1 instance connected to account
      // to avoid situation with issues in different instances
      const issueKeys = issues.map(i => i.title).join(', ')

      return res.send({
        results: issues.slice(0, ISSUES_LIMIT),
        totalCount: issues.length,
        itemLabel: 'issues reported by this contact',
        allItemsLink: encodeURI(`${req.context.Instance.baseUrl}/issues/?jql=key in (${issueKeys})`),
      })
    }

    if (issues.length === 0) {
      return res.send({
        results: [{
          objectId: null,
          title: `There are no Jira issues associated with this ${recordType}.`,
        }],
      })
    }

    res.send({
      results: issues.slice(0, ISSUES_LIMIT),
    })
  }
}
