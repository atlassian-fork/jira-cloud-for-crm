const Statsd = require('hot-shots')
const config = require('../../common/config')

const serviceConfig = config.get('service')
const { host, port } = config.get('statsd')

const servicePrefix = `${config.get('service.name')}.`
const isLocal = config.get('service.environment') !== 'production'

function buildClient (logger) {
  if (isLocal) {
    const sendMessage = (...args) => logger.info(...args)

    return {
      increment: sendMessage,
      decrement: sendMessage,
      timing: sendMessage,
      gauge: sendMessage,
    }
  }

  const client = new Statsd({
    host,
    port,
    prefix: servicePrefix,
    globalTags: {
      environment: serviceConfig.microsEnvironment,
      environment_type: serviceConfig.microsEnvironmentType,
      deployment_id: serviceConfig.microsDeploymentId,
      region: serviceConfig.microsAWSRegion,
      micros_group: serviceConfig.microsGroup,
      version: serviceConfig.microsVersion,
    },
  })

  client.socket.on('error', error => logger.error({ err: error }, 'Statsd error'))

  return client
}

module.exports = (logger) => {
  const client = buildClient(logger)

  return {
    increment (key, tags) {
      client.increment(key, 1, buildTagArray(tags))
    },
    decrement (key, tags) {
      client.decrement(key, -1, buildTagArray(tags))
    },
    timing (key, time, tags) {
      client.timing(key, time, buildTagArray(tags))
    },
    gauge (key, time, tags) {
      client.gauge(key, time, buildTagArray(tags))
    },
  }
}

function buildTagArray (tagObject = {}) {
  return Object.keys(tagObject).map(key => `${key}:${tagObject[key]}`)
}
