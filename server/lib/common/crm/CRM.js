const dynamicsDefaultSchema = require('./Dynamics/defaultSchema.json')
const hubspotDefaultSchema = require('./Hubspot/defaultSchema.json')
const salesforceDefaultSchema = require('./Salesforce/defaultSchema.json')

const defaultSchemas = {
  Dynamics: dynamicsDefaultSchema,
  Hubspot: hubspotDefaultSchema,
  Salesforce: salesforceDefaultSchema,
}

class CRM {
  constructor (api, account) {
    const crmProviderName = this.constructor.name // child class name

    this.account = account
    this.api = api
    this.crmApi = api(crmProviderName, account)
    this.defaultSchema = defaultSchemas[crmProviderName]
  }

  getDefaultSchema () {
    return this.defaultSchema
  }

  async getFields (schema = this.defaultSchema) {
    const promises = schema.map(async (parentObject) => {
      const fields = await this.getParentFields(parentObject.id)
      const { objects } = this.defaultSchema.find(o => o.id === parentObject.id)

      return { ...parentObject, fields, objects }
    })

    return Promise.all(promises)
  }

  async getValues (schema, recordId, recordType, childRecordType) {
    if (childRecordType) {
      // handle customer's child object data load
      const parentSchema = schema.find(group => group.id === recordType)
      const childSchema = parentSchema.objects.filter(group => group.id === childRecordType)

      const data = await this.getChildValues(childSchema, recordId, recordType, childRecordType)

      if (!data) return

      return this.mapCustomerData(childSchema, data, true)
    }

    // handle top-level customer object
    const data = await this.getParentValues(schema, recordId, recordType)

    if (!data) return

    return this.mapCustomerData(schema, data)
  }

  healthcheck () {
    return this.apiCheck()
  }
}

module.exports = CRM
