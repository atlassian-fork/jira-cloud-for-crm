const AWS = require('aws-sdk')

const {
  service: { environment },
  sqs: { region, endpoint, queues },
} = require('../config').get()

const config = {
  region,
  httpOptions: {
    timeout: 25000, // https://github.com/aws/aws-sdk-js/issues/367#issuecomment-56875656
  },
}

/* istanbul ignore if */
if (environment === 'production') {
  config.credentials = new AWS.EC2MetadataCredentials()
} else {
  config.endpoint = endpoint
}

const client = new AWS.SQS(config)

const sendMessage = (queueName, message) => client
  .sendMessage({
    QueueUrl: queues[queueName].url,
    MessageBody: JSON.stringify(message),
  })
  .promise()

module.exports = {
  client,
  sendMessage,
}
