const moment = require('moment')
const client = require('./client')('streamhub')
const logger = require('../logger').child({ module: 'streamhub' })
const config = require('../config')

const { microsEnvironment, microsServiceName } = config.get('service')
const { issuer, privateKey } = config.get('jwt')

const baseUrl = microsEnvironment.startsWith('prod-')
  ? 'https://streamhub-api.prod.atl-paas.net'
  : 'https://streamhub-api.staging.atl-paas.net'

const isAsapEnabled = issuer && privateKey
const tokenFactory = false

class StreamHub {
  acknowledgeEvent ({ accountId, correlationId, status, errorMessage = '' }) {
    // Schema https://goo.gl/WMLgzU
    const body = {
      type: 'avi:gdpr:updated:gdpr_rtbf_acknowledgement',
      schema: 'it-bdm/gdpr-rtbf-acknowledgement-v1.2.json',
      resource: `ari:cloud:identity::user/${accountId}`,
      payload: {
        status,
        source: `micros/${microsServiceName}`,
        correlationId,
        timestamp: moment().toISOString(),
        ...errorMessage && { errorMessage },
      },
    }

    return this._fetch(
      'acknowledgeEvent',
      { pathname: '/event/record' },
      { method: 'POST', body }
    )
  }

  async _fetch (
    methodName,
    { pathname, query },
    { method = 'GET', body, headers = {} } = {}
  ) {
    if (!isAsapEnabled) {
      return logger.info('ASAP is not available locally')
    }

    const url = new URL(pathname, baseUrl)

    if (query) {
      Object.entries(query)
        .map(([key, value]) => url.searchParams.append(key, value))
    }

    if (headers['Content-Type'] === undefined) {
      headers['Content-Type'] = 'application/json'
    }

    if (headers.Authorization === undefined) {
      headers.Authorization = tokenFactory.createAsapTokenHeader()
    }

    if (body && headers['Content-Type'] === 'application/json') {
      body = JSON.stringify(body)
    }

    const state = {
      req: {
        method,
        headers,
        body,
        url: url.toString(),
      },
      res: {},
    }

    try {
      await client(state, methodName)

      return state.res.body
    } catch (error) {
      const fields = {
        originError: error,
        source: 'streamhub',
      }

      throw Object.assign(
        new Error('StreamHub API error'),
        state,
        fields,
      )
    }
  }
}

module.exports = StreamHub

module.exports.statuses = {
  notFound: 'NOT_FOUND',
  success: 'SUCCESS',
  failure: 'FAILURE',
}
