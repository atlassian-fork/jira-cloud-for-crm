/* eslint-disable global-require */

const api = {
  Dynamics: require('./Dynamics'),
  Hubspot: require('./Hubspot'),
  Jira: require('./Jira'),
  Salesforce: require('./Salesforce'),
  StreamHub: require('./StreamHub'),
}

const initCacher = require('./cacher')

module.exports = function initNet (cache, logger) {
  const getHandler = initCacher(cache, logger)

  return (serviceName, params) => {
    const Class = api[serviceName]

    const target = new Class(params)
    const handler = getHandler(serviceName)

    return new Proxy(target, handler)
  }
}
