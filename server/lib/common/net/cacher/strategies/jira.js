const hash = require('object-hash')

module.exports = {
  allowedToCache (target, property) {
    if (property.startsWith('get')) {
      return true
    }
  },
  getKey ({ target, property, params }) {
    return hash({
      instanceId: target.instance.id,
      userToken: target.userToken || '',
      property,
      params,
    })
  },
}
