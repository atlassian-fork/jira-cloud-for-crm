const Service = require('../Service')
const ServiceError = require('../Error')
const { getProjectAdminPermission } = require('./util')
const { sendMessage } = require('../../common/sqs/client')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      id: ['required', 'positive_integer'],
    }

    return this.validator(params, rules)
  }

  async execute ({ id }) {
    const connection = await this.model.Connection.findByPk(id, {
      include: [{
        model: this.model.Instance,
        where: { id: this.context('instance.id') },
      }, {
        model: this.model.Account,
        required: true,
      }],
    })

    if (!connection) {
      throw new ServiceError('Wrong connection id', {
        code: 'WRONG_ID',
        fields: ['id'],
      })
    }

    const hasProjectAdminPermission = await getProjectAdminPermission(
      this.context(),
      connection.projectId
    )

    if (!hasProjectAdminPermission) {
      throw new ServiceError('You have no access to project related to this connection', {
        code: 'PERMISSION_DENIED',
      })
    }

    await connection.update({
      totalIssues: 0,
      checkedIssues: 0,
      matchedIssues: 0,
    })

    await sendMessage('GetIssues', {
      data: {
        connectionId: connection.id,
        projectId: connection.projectId,
      },
      context: this.context(),
    })

    return { id }
  }
}
