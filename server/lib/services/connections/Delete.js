const Service = require('../Service')
const ServiceError = require('../Error')
const { getProjectAdminPermission } = require('./util')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      id: ['required', 'positive_integer'],
    }

    return this.validator(params, rules)
  }

  async execute ({ id }) {
    await this.model.sequelize.transaction(async (t) => {
      const connection = await this.model.Connection.findByPk(id, {
        include: [{
          model: this.model.Instance,
          where: { id: this.context('instance.id') },
        }, {
          model: this.model.Account,
          required: true,
        }],
        transaction: t,
      })

      if (!connection) {
        throw new ServiceError('Wrong connection id', {
          code: 'WRONG_ID',
          fields: ['id'],
        })
      }

      const hasProjectAdminPermission = await getProjectAdminPermission(
        this.context(),
        connection.projectId
      )

      if (!hasProjectAdminPermission) {
        throw new ServiceError('You have no access to project related to this connection', {
          code: 'PERMISSION_DENIED',
        })
      }

      const { accountId } = connection

      await this.model.Connection.destroy({
        where: { id: connection.id },
        transaction: t,
        force: true,
      })

      this.logger.info({ id: connection.id }, 'Connection was deleted')

      const jira = this.api('Jira', this.context('instance'))

      await jira.deleteProjectProperty(connection.projectId)

      const account = await this.model.Account.findByPk(accountId, {
        include: {
          model: this.model.Connection,
        },
        transaction: t,
      })

      if (!account.Connections.length) {
        await account.destroy({
          force: true,
          transaction: t,
        })

        this.logger.info({ id: accountId }, 'Account was deleted')
      }
    })

    return true
  }
}
