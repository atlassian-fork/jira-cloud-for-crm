const { get } = require('lodash')
const getCRMClass = require('../../../common/crm')
const Service = require('../../Service')
const validationRules = require('./validationRules')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      issue: ['required', validationRules.issue],
    }

    return this.validator(params, rules)
  }

  async execute ({ issue }) {
    const reporter = get(issue, 'fields.reporter')

    const jira = this.api('Jira', this.context('instance'))
    const email = reporter && (
      reporter.emailAddress || await jira.getUserEmail(reporter.accountId)
    )

    if (!email) return

    const connection = await this.model.Connection.findOne({
      include: {
        model: this.model.Account,
        required: true,
      },
      where: {
        instanceId: this.context('instance.id'),
        projectId: issue.fields.project.id,
      },
    })

    if (!connection || !connection.Account.isAuthorized) return

    const CRM = getCRMClass(connection.Account.type)
    const crm = new CRM(this.api.bind(this), connection.Account)

    try {
      const matchContactId = await crm.findMatchContactId(email)

      if (matchContactId) {
        await this.model.Match.create({
          recordType: 'Contact',
          recordId: matchContactId,
          connectionId: connection.id,
          issueId: issue.id,
        })
      }
    } catch (error) {
      if (error.code !== 'INVALID_CLIENT') {
        this.logger.warn(
          { err: error },
          'Error while getting contact in issue.created'
        )
      }
    }
  }
}
