const Service = require('../../Service')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      id: ['required', 'string'],
    }

    return this.validator(params, rules)
  }

  async execute ({ id }) {
    await this.model.Account.restore({
      where: { id },
    })
  }
}
