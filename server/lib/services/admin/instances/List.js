const Service = require('../../Service')

const { sortableBy, sortBy } = require('../utils')

const orderFields = sortableBy([
  'baseUrl',
  'description',
  'serverVersion',
  'pluginsVersion',
  'createdAt',
  'deletedAt',
])

module.exports = class extends Service {
  validate (params) {
    const rules = {
      limit: ['positive_integer'],
      offset: ['integer', { min_number: 0 }],
      where: [{ nested_object: {
        id: [{ list_of: ['string'] }],
        description: ['string'],
        baseUrl: ['string'],
      } }],
      order: { list_of: {
        one_of: orderFields,
      } },
    }

    return this.validator(params, rules)
  }

  async execute ({ limit = 50, offset = 0, where = {}, order = [] }) {
    if (where.id && where.id.length) {
      where.id = { $in: where.id }
    } else {
      delete where.id
    }

    if (where.description) {
      where.description = { $iLike: `%${where.description}%` }
    } else {
      delete where.description
    }

    if (where.baseUrl) {
      where.baseUrl = { $iLike: `%${where.baseUrl}%` }
    } else {
      delete where.baseUrl
    }

    const { rows, count } = await this.model.Instance.findAndCountAll({
      limit,
      offset,
      where,
      paranoid: false,
      attributes: [
        'id',
        'baseUrl',
        'description',
        'serverVersion',
        'pluginsVersion',
        'createdAt',
        'deletedAt',
      ],
      order: sortBy(order),
    })

    return {
      count,
      instances: rows,
    }
  }
}
