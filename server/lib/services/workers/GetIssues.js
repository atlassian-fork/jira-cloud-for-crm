const Service = require('../Service')
const { sendMessage } = require('../../common/sqs/client')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      connectionId: ['required', 'positive_integer'],
      projectId: ['required', 'string'],
    }

    return this.validator(params, rules)
  }

  async execute ({ connectionId, projectId }) {
    try {
      const jira = this.api('Jira', this.context('instance'))

      await this._getIssues({
        jira,
        connectionId,
        projectId,
      })

      this.logger.info(
        { connectionId, projectId },
        'All issues are retrieved and passed to SyncIssue worker'
      )
    } catch (error) {
      this.logger.error(
        { err: error, connectionId, projectId },
        'Retrieving of issues are failed'
      )

      throw error
    }
  }

  async _getIssues ({ jira, connectionId, projectId }, startAt = 0) {
    const { issues, total } = await jira.getIssues({
      jql: `project = ${projectId} ORDER BY id DESC`,
      fields: ['reporter'],
      maxResults: 100,
      startAt,
    })

    if (!issues) return

    await this.model.Connection.increment('totalIssues', {
      by: issues.length,
      where: { id: connectionId },
    })

    for (const issue of issues) {
      await sendMessage('SyncIssue', {
        data: {
          issue,
          connectionId,
        },
        context: this.context(),
      })
    }

    startAt += 100

    if (startAt >= total) return

    await this._getIssues({ jira, connectionId, projectId }, startAt)
  }
}
