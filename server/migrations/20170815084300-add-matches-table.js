module.exports = {
  async up (queryInterface, DataTypes) {
    await queryInterface.createTable('Matches', {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      connectionId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Connections',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      recordType: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      recordId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      jiraUserKey: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    })

    const fieldsToIndex = [
      'connectionId',
      'jiraUserKey',
      'createdAt',
      'updatedAt',
      'deletedAt',
    ]

    for (const field of fieldsToIndex) {
      await queryInterface.addIndex('Matches', [field])
    }
  },

  async down (queryInterface) {
    await queryInterface.dropTable('Matches')
  },
}
