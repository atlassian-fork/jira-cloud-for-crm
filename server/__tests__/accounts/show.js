const { loadFixtures } = require('sequelize-fixtures')
const nock = require('nock')
const { Context, fixtures, mocks, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts, ...fixtures.connections],
  context.model
))

describe('Accounts.Show', () => {
  test('Get Salesforce account information for specific project', async () => {
    mocks.salesforce.listeners.getUserInfo()
    mocks.salesforce.listeners.getContactFieldsMetadata()
    mocks.salesforce.listeners.getAccountFieldsMetadata()
    mocks.salesforce.listeners.getLeadFieldsMetadata()

    const accountFields = fixtures.accounts[0].data
    const { projectId } = fixtures.connections[0].data

    const { body } = await context.client
      .get('/api/accounts')
      .query({ projectId })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')
    expect(body).toHaveProperty('data.user')
    expect(body).toHaveProperty('data.connection')

    expect(body.data.id).toBe(accountFields.id)
    expect(body.data.type).toBe('salesforce')
    expect(body.data.user).toEqual({
      name: 'John Doe',
      email: 'test@mail.com',
    })

    const [contact, lead, account] = body.data.fields

    expect(contact.fields).toEqual([
      { id: 'Name', label: 'Full Name', type: 'title' },
      { id: 'Email', label: 'Email', type: 'email' },
      { id: 'TestField1', label: 'Test Field', type: 'string' },
    ])
    expect(lead.fields).toEqual([
      { id: 'Name', label: 'Full Name', type: 'title' },
      { id: 'Email', label: 'Email', type: 'email' },
      { id: 'TestField1', label: 'Test Field', type: 'string' },
    ])
    expect(account.fields).toEqual([
      { id: 'Type', label: 'Account Type', type: 'picklist' },
      { id: 'TestField1', label: 'Test Field', type: 'string' },
    ])
  })

  test('Get MS Dynamics account information for specific project', async () => {
    mocks.dynamics.listeners.getMyself()
    mocks.dynamics.listeners.getContactEntityDefinition()
    mocks.dynamics.listeners.getAccountEntityDefinition()

    const accountFields = fixtures.accounts[1].data
    const { projectId } = fixtures.connections[1].data

    const { body } = await context.client
      .get('/api/accounts')
      .query({ projectId })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')
    expect(body).toHaveProperty('data.user')
    expect(body).toHaveProperty('data.connection')

    expect(body.data.id).toBe(accountFields.id)
    expect(body.data.type).toBe('dynamics')
    expect(body.data.user).toEqual({
      name: 'Test Dynamics-User',
      email: 'test@dynamics.com',
    })

    const [contact, account] = body.data.fields

    expect(contact.fields).toEqual([
      { id: 'emailaddress1', label: 'Email', type: 'string' },
      { id: 'fullname', label: 'Full Name', type: 'title' },
      { id: 'contacttestfield', label: 'Contact Test Field', type: 'string' },
    ])
    expect(account.fields).toEqual([
      { id: 'emailaddress1', label: 'Email', type: 'string' },
      { id: 'fullname', label: 'Full Name', type: 'title' },
      { id: 'accounttestfield', label: 'Account Test Field', type: 'string' },
    ])
  })

  test('Get Hubspot account information for specific project', async () => {
    mocks.hubspot.listeners.getTokenInfo()
    mocks.hubspot.listeners.getContactProperties()
    mocks.hubspot.listeners.getCompanyProperties()

    const accountFields = fixtures.accounts[2].data
    const { projectId } = fixtures.connections[2].data

    const { body } = await context.client
      .get('/api/accounts')
      .query({ projectId })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')
    expect(body).toHaveProperty('data.user')
    expect(body).toHaveProperty('data.connection')

    expect(body.data.id).toBe(accountFields.id)
    expect(body.data.type).toBe('hubspot')
    expect(body.data.user).toEqual({ email: 'test@mail.com' })

    const [contact, account] = body.data.fields

    expect(contact.fields).toEqual([
      { id: 'firstname', label: 'First Name', type: 'string' },
      { id: 'lastname', label: 'Last Name', type: 'string' },
      { id: 'email', label: 'Email', type: 'string' },
      { id: 'createdate', label: 'Create Date', type: 'datetime' },
      { id: 'hubspot_owner_id', label: 'HubSpot Owner', type: 'enumeration' },
      { id: 'jobtitle', label: 'Job Title', type: 'string' },
    ])
    expect(account.fields).toEqual([
      { id: 'name', label: 'Name', type: 'text' },
      { id: 'testfield', label: 'Company Test Field', type: 'text' },
    ])
  })

  test('Failed to get Salesforce account information', async () => {
    mocks.salesforce.listeners.getUserInfo()
    mocks.salesforce.listeners.getContactFieldsMetadata()
    mocks.salesforce.listeners.getAccountFieldsMetadata()

    const { baseUrl } = fixtures.accounts[0].data

    nock(baseUrl)
      .get('/services/data/v40.0/sobjects/Lead/describe')
      .reply(400, () => ({
        error: 'invalid_grant',
        error_description: 'inactive user',
      }))

    const { projectId } = fixtures.connections[0].data

    const { body } = await context.client
      .get('/api/accounts')
      .query({ projectId })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body.ok).toBeTruthy()
    expect(body.data.user).toEqual({})
    expect(body.data.needsAuthorization).toBeTruthy()
  })

  test('No account was found, show initial page', async () => {
    await context.model.Account.destroy({
      where: { id: 1 },
      force: true,
    })

    const { body } = await context.client
      .get('/api/accounts')
      .query({ projectId: 1000 })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
  })
})
