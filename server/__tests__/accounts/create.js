const uuid = require('uuid/v4')
const jwt = require('atlassian-jwt')
const { loadFixtures } = require('sequelize-fixtures')
const { Context, fixtures, mocks, utils } = require('../helpers')

const csrf = uuid()
const csrfCacheKey = 'csrf:test:account-id'

const context = new Context()

beforeAll(async () => {
  await context.begin()
  await context.model.sequelize.cache.setAsync(csrfCacheKey, csrf, 300)
  await loadFixtures(
    [...fixtures.instance],
    context.model
  )
})

afterAll(async () => {
  await context.model.sequelize.cache.delAsync(csrfCacheKey)
  await context.end()
})

describe('Accounts.Create', () => {
  test('Sign In with Salesforce', async () => {
    mocks.salesforce.listeners.getBearerToken()
    mocks.salesforce.listeners.getUserInfo()
    mocks.salesforce.listeners.getContactFieldsMetadata()

    const response = await context.client
      .get('/api/accounts/create/salesforce/callback')
      .set('Cookie', [`csrf=${csrf}`])
      .query({
        state: utils.auth.jira(),
        code: 'dummy',
      })
      .expect(302)

    expect(response.headers.location).toMatch('http://localhost:8080/account_connected?accountId')
    const { searchParams } = new URL(response.headers.location)
    const accountId = searchParams.get('accountId')

    expect(accountId).toBeTruthy()

    const instance = fixtures.instance[0].data
    const salesforceInfo = mocks.salesforce.responses.userInfo

    const account = await context.model.Account.findByPk(accountId)

    expect(account.type).toBe('salesforce')
    expect(account.jiraAccountId).toBe('test:account-id')
    expect(account.instanceId).toBe(instance.id)
    expect(account.baseUrl).toBe(salesforceInfo.profile.substring(0, salesforceInfo.profile.lastIndexOf('/')))
    expect(account.externalId).toBe(salesforceInfo.organization_id)
    expect(account.crmUserId).toBe(salesforceInfo.user_id)
  })

  test('Sign In with MS Dynamics', async () => {
    mocks.dynamics.listeners.getBearerToken()
    mocks.dynamics.listeners.whoAmI()

    const response = await context.client
      .get('/api/accounts/create/dynamics/callback')
      .set('Cookie', [`csrf=${csrf}`])
      .query({
        state: utils.auth.jira(),
        code: 'dummy',
      })
      .expect(302)

    expect(response.headers.location).toMatch('http://localhost:8080/account_connected?accountId')

    const { searchParams } = new URL(response.headers.location)
    const accountId = searchParams.get('accountId')

    expect(accountId).toBeTruthy()

    const instance = fixtures.instance[0].data
    const profileInfo = jwt.decode(mocks.dynamics.responses.accessToken.access_token, null, true)
    const accountInfo = mocks.dynamics.responses.whoAmI

    const account = await context.model.Account.findByPk(accountId)

    expect(account.type).toBe('dynamics')
    expect(account.jiraAccountId).toBe('test:account-id')
    expect(account.instanceId).toBe(instance.id)
    expect(account.baseUrl).toBe(profileInfo.aud[0])
    expect(account.externalId).toBe(accountInfo.OrganizationId)
    expect(account.crmUserId).toBe(accountInfo.UserId)
  })

  test('Sign In with Hubspot', async () => {
    mocks.hubspot.listeners.getBearerToken()
    mocks.hubspot.listeners.getTokenInfo()

    const response = await context.client
      .get('/api/accounts/create/hubspot/callback')
      .set('Cookie', [`csrf=${csrf}`])
      .query({
        state: utils.auth.jira(),
        code: 'dummy',
      })
      .expect(302)

    expect(response.headers.location).toMatch('http://localhost:8080/account_connected?accountId')

    const { searchParams } = new URL(response.headers.location)
    const accountId = searchParams.get('accountId')

    expect(accountId).toBeTruthy()

    const instance = fixtures.instance[0].data
    const { tokenInfo } = mocks.hubspot.responses

    const account = await context.model.Account.findByPk(accountId)

    expect(account.type).toBe('hubspot')
    expect(account.jiraAccountId).toBe('test:account-id')
    expect(account.instanceId).toBe(instance.id)
    expect(account.baseUrl).toBe(`https://${tokenInfo.hub_domain}`)
    expect(account.externalId).toBe(String(tokenInfo.hub_id))
    expect(account.crmUserId).toBe(String(tokenInfo.user_id))
  })

  test('Update account', async () => {
    mocks.salesforce.listeners.getBearerToken()
    mocks.salesforce.listeners.getUserInfo()

    await context.client
      .get('/api/accounts/create/salesforce/callback')
      .set('Cookie', [`csrf=${csrf}`])
      .query({
        state: utils.auth.jira(),
        code: 'dummy',
      })
      .expect(302)
  })

  test('API is disabled for this organization', async () => {
    mocks.salesforce.listeners.getBearerToken()
    mocks.salesforce.listeners.getUserInfo()
    mocks.salesforce.listeners.failedHealthcheck()

    await context.model.Account.destroy({
      where: { id: 1 },
      force: true,
    })

    const response = await context.client
      .get('/api/accounts/create/salesforce/callback')
      .set('Cookie', [`csrf=${csrf}`])
      .query({
        state: utils.auth.jira(),
        code: 'dummy',
      })
      .expect(302)

    expect(response.headers.location).toMatch('/error?code=API_DISABLED_FOR_ORG&message=The%20REST%20API%20is%20not%20enabled%20for%20this%20Organization.')
  })
})
