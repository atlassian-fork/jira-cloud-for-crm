const AWS = require('aws-sdk')
const config = require('../../../lib/common/config')

class SQS {
  constructor (testId) {
    const endpoint = config.get('sqs.endpoint')

    this.queuesConfig = Object.keys(config.get('sqs.queues')).map((queueType) => {
      const name = `test-${testId}-${queueType}`
      const url = `${endpoint}/queue/${name}`

      return { name, url, queueType }
    })

    const sqsConfig = {
      region: config.get('sqs.region'),
      endpoint: config.get('sqs.endpoint'),
    }

    this.sqsClient = new AWS.SQS(sqsConfig)
  }

  async init () {
    // needed for aws-sdk module
    // http://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-credentials-node.html
    const awsKeys = ['AWS_ACCESS_KEY_ID', 'AWS_SECRET_ACCESS_KEY', 'AWS_SESSION_TOKEN']

    awsKeys.forEach((key) => {
      process.env[key] = 'DUMMY'
    })

    const createQueue = queueConfig => this.sqsClient
      .createQueue({ QueueName: queueConfig.name })
      .promise()

    const setQueueConfig = (queueConfig) => {
      config.set(`sqs.queues.${queueConfig.queueType}.name`, queueConfig.name)
      config.set(`sqs.queues.${queueConfig.queueType}.url`, queueConfig.url)
    }

    await Promise.all(this.queuesConfig.map(createQueue))
    this.queuesConfig.map(setQueueConfig)

    return this
  }

  async clean () {
    const deleteQueue = queueConfig => this.sqsClient
      .deleteQueue({ QueueUrl: queueConfig.url })
      .promise()

    await Promise.all(this.queuesConfig.map(deleteQueue))
  }
}

module.exports = SQS
