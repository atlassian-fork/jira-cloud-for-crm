const pg = require('pg')
const { promisify } = require('util')
const { tryWhile } = require('../../../lib/services/util')
const config = require('../../../lib/common/config')

class Postgres {
  constructor (testId) {
    this.database = `test_db_${testId.replace(/-/g, '_')}`
  }

  async init () {
    const getClient = async () => {
      try {
        const options = config.get('postgres.write')
        const client = new pg.Client({
          user: options.username,
          ...options,
        })

        await promisify(client.connect.bind(client))()

        return client
      } catch (error) {
        return false
      }
    }

    const createDatabase = async () => {
      try {
        await this.client.query(`CREATE DATABASE ${this.database}`)

        return true
      } catch (error) {
        return false
      }
    }

    this.client = await tryWhile(getClient, result => result, 5000, 5)
    await tryWhile(createDatabase, null, 1000, 3)

    config.set('postgres.read.database', this.database)
    config.set('postgres.write.database', this.database)

    return this
  }

  async clean () {
    if (!this.client) return
    // force drop of all db connections
    await this.client.query(`
    SELECT pg_terminate_backend(pg_stat_activity.pid)
    FROM pg_stat_activity
    WHERE pg_stat_activity.datname = '${this.database}'
      AND pid <> pg_backend_pid();
    `)
    await this.client.query(`DROP DATABASE ${this.database}`)
    await this.client.end()
  }
}

module.exports = Postgres
