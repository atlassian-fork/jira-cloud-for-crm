const config = require('../../../lib/common/config')

class Memcached {
  constructor (testId) {
    this.namespace = `test-${testId}`
  }

  async init () {
    config.set('memcached.namespace', this.namespace)

    return this
  }

  async clean () {
    // cleaning all memcached records is tricky
    // so they will be deleted after TTL expire
  }
}

module.exports = Memcached
