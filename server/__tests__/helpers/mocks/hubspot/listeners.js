const nock = require('nock')
const requireDirectory = require('require-directory')

const { responses } = requireDirectory(module)
const baseUrl = 'https://api.hubapi.com'

function getBearerToken () {
  return nock(baseUrl)
    .post('/oauth/v1/token')
    .query(true)
    .reply(200, responses.accessToken)
}

function getTokenInfo (accessToken = responses.accessToken.access_token) {
  return nock(baseUrl)
    .get(`/oauth/v1/access-tokens/${accessToken}`)
    .reply(200, responses.tokenInfo)
}

function getContactProperties () {
  return nock(baseUrl)
    .get('/properties/v1/contacts/properties')
    .reply(200, responses.contactProperties)
}

function getCompanyProperties () {
  return nock(baseUrl)
    .get('/properties/v1/companies/properties')
    .reply(200, responses.companyProperties)
}

function getContactByEmail () {
  return nock(baseUrl)
    .get('/contacts/v1/contact/email/test@mail.com/profile')
    .reply(200, responses.contact)
}

function getContactById () {
  return nock(baseUrl)
    .get('/contacts/v1/contact/vid/101/profile')
    .query({ properties: 'email' })
    .reply(200, responses.contact)
}

function getContactById404 () {
  return nock(baseUrl)
    .get('/contacts/v1/contact/vid/101/profile')
    .query({ properties: 'email' })
    .reply(404, {
      status: 'error',
      message: 'contact does not exist',
    })
}

function getContactList () {
  return nock(baseUrl)
    .get('/contacts/v1/search/query')
    .query({ q: 'd' })
    .reply(200, responses.contactList)
}

function getDealsByContact () {
  return nock(baseUrl)
    .get('/deals/v1/deal/associated/contact/101/paged')
    .query({
      properties: ['pipeline', 'dealname', 'dealstage', 'closedate'],
      limit: 3,
    })
    .reply(200, responses.dealsByContact)
}

function getPipelineById () {
  return nock(baseUrl)
    .get('/deals/v1/pipelines/default')
    .times(2)
    .reply(200, responses.pipeline)
}

function getContactsByCompany () {
  return nock(baseUrl)
    .get('/companies/v2/companies/100000000/contacts')
    .query({
      properties: ['name', 'email'],
      count: 3,
    })
    .reply(200, responses.contactsByCompany)
}

module.exports = {
  getBearerToken,
  getTokenInfo,
  getContactProperties,
  getCompanyProperties,
  getContactByEmail,
  getContactById,
  getContactById404,
  getContactList,
  getDealsByContact,
  getContactsByCompany,
  getPipelineById,
}
