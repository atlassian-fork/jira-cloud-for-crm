const nock = require('nock')
const requireDirectory = require('require-directory')
const { URL } = require('url')

const { responses } = requireDirectory(module)

const baseUrl = (new URL(responses.userInfo.profile)).origin

function getBearerToken () {
  return nock('https://login.salesforce.com')
    .post('/services/oauth2/token')
    .query(true)
    .reply(200, responses.accessToken)
}

function getUserInfo () {
  return nock('https://login.salesforce.com/')
    .get('/services/oauth2/userinfo')
    .query(true)
    .reply(200, responses.userInfo)
}

function failedHealthcheck () {
  return nock(baseUrl)
    .get('/services/data/v40.0/sobjects/Contact/describe')
    .reply(403, responses.healthcheckAPIDisabled)
}

function getContactFieldsMetadata () {
  return nock(baseUrl)
    .get('/services/data/v40.0/sobjects/Contact/describe')
    .reply(200, responses.contactMetadata)
}

function getAccountFieldsMetadata () {
  return nock(baseUrl)
    .get('/services/data/v40.0/sobjects/Account/describe')
    .reply(200, responses.accountMetadata)
}

function getLeadFieldsMetadata () {
  return nock(baseUrl)
    .get('/services/data/v40.0/sobjects/Lead/describe')
    .reply(200, responses.leadMetadata)
}

function getLeadFieldsMetadataNotFound () {
  return nock(baseUrl)
    .get('/services/data/v40.0/sobjects/Lead/describe')
    .reply(404)
}

function getContactByEmail (email = 'test@mail.com') {
  return nock(baseUrl)
    .get('/services/data/v40.0/query')
    .query({
      q: `select Id, Name, Email from Contact where email = '${email}'`,
    })
    .reply(200, responses.contact)
}

function getContactById () {
  return nock(baseUrl)
    .get('/services/data/v40.0/query')
    .query({
      q: 'select Id, Name, Account.Id, Account.Name, Contact.Email, Account.Type from Contact where id = \'CONTACTID0\'',
    })
    .reply(200, responses.contactWithAccount)
}

function getContactList () {
  return nock(baseUrl)
    .get('/services/data/v40.0/query')
    .query({
      q: 'select Id, Name, Email, PhotoUrl, Account.Name from Contact where FirstName like \'d%\' or LastName like \'d%\' or Email like \'d%\'',
    })
    .reply(200, responses.contactList)
}

function getAccountList () {
  return nock(baseUrl)
    .get('/services/data/v40.0/query')
    .query({
      q: 'select Id, Name, PhotoUrl from Account where Name like \'%d%\'',
    })
    .reply(200, responses.accountList)
}

function getLeadList () {
  return nock(baseUrl)
    .get('/services/data/v40.0/query')
    .query({
      q: 'select Id, Name, Email, PhotoUrl from Lead where FirstName like \'d%\' or LastName like \'d%\' or Email like \'d%\'',
    })
    .reply(200, responses.leadList)
}

function getLeadListNotFound () {
  return nock(baseUrl)
    .get('/services/data/v40.0/query')
    .query({
      q: 'select Id, Name, Email, PhotoUrl from Lead where FirstName like \'d%\' or LastName like \'d%\' or Email like \'d%\'',
    })
    .reply(404)
}

function getCasesByContact () {
  return nock(baseUrl)
    .get('/services/data/v40.0/query')
    .query({
      q: 'select (select Case.Id, Case.CaseNumber, Case.Subject, Case.Status from Contact.Cases order by LastModifiedDate desc limit 3) from Contact where Id = \'CONTACTID0\'',
    })
    .reply(200, responses.casesByContact)
}

function getOpportunitiesByContact () {
  return nock(baseUrl)
    .get('/services/data/v40.0/query')
    .query({
      q: 'select (select OpportunityContactRole.Opportunity.Id, OpportunityContactRole.Opportunity.Name, OpportunityContactRole.Opportunity.StageName, OpportunityContactRole.Opportunity.CloseDate from Contact.OpportunityContactRoles order by LastModifiedDate desc limit 3) from Contact where Id = \'CONTACTID0\'',
    })
    .reply(200, responses.opportunitiesByContact)
}

function getEntitlementsByAccount () {
  return nock(baseUrl)
    .get('/services/data/v40.0/query')
    .query({
      q: 'select (select Entitlement.Id, Entitlement.Name, Asset.Id, Asset.Name, Entitlement.Status, Entitlement.Type, Entitlement.StartDate, Entitlement.EndDate, Entitlement.AssetId from Account.Entitlements order by LastModifiedDate desc limit 3) from Account where Id = \'ACCOUNTID0\'',
    })
    .reply(200, responses.entitlementsByAccount)
}

function entitlementsIsDisabled () {
  return nock(baseUrl)
    .get('/services/data/v40.0/query')
    .query({
      q: 'select (select Entitlement.Id, Entitlement.Name, Asset.Id, Asset.Name, Entitlement.Status, Entitlement.Type, Entitlement.StartDate, Entitlement.EndDate, Entitlement.AssetId from Account.Entitlements order by LastModifiedDate desc limit 3) from Account where Id = \'ACCOUNTID0\'',
    })
    .reply(403, responses.entitlementsDisabled)
}

module.exports = {
  getBearerToken,
  getUserInfo,
  failedHealthcheck,
  getContactFieldsMetadata,
  getAccountFieldsMetadata,
  getLeadFieldsMetadata,
  getLeadFieldsMetadataNotFound,
  getContactByEmail,
  getContactById,
  getContactList,
  getAccountList,
  getLeadList,
  getLeadListNotFound,
  getCasesByContact,
  getOpportunitiesByContact,
  getEntitlementsByAccount,
  entitlementsIsDisabled,
}
