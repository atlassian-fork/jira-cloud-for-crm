import React, { Component } from 'react'
import { shape, object, array, bool, string, element } from 'prop-types'
import gas from '../../services/gas'

import Button from '@atlaskit/button'
import DropdownMenu, { DropdownItemGroup, DropdownItem } from '@atlaskit/dropdown-menu'
import ShortcutIcon from '@atlaskit/icon/glyph/shortcut'
import MoreIcon from '@atlaskit/icon/glyph/more'

import {
  Wrapper,

  HeaderWrapper,
  HeaderContent,
  HeaderTitle,
  HeaderActionsWrapper,

  ContentWrapper,
  RelatedsWrapper,
  FieldWrapper,
  FieldTitle
} from './styled/Card'
import Chevron from './Chevron'
import HeadIcon from './Icon'

export default class Card extends Component {
  static propTypes = {
    appearance: string.isRequired,
    head: shape({
      title: string.isRequired,
      icon: string,
      iconLabel: string,
      link: string,
      actions: array,
      analytics: object
    }),
    isPreview: bool.isRequired,
    values: array.isRequired,
    childObjects: element,
    items: array,
    type: string
  }

  static defaultProps = {
    appearance: 'default',
    head: {
      title: '',
      actions: []
    },
    isPreview: false,
    items: null
  }

  state = {
    isContentOpen: this.props.isPreview
  }

  toggleContent = () => {
    this.setState(prevState => ({
      isContentOpen: !prevState.isContentOpen
    }))

    const { head: { analytics: { provider, recordType } } } = this.props

    gas.send({
      page: 'web-panel',
      name: `crm.${provider}.parent.${recordType}`
    })
  }

  render () {
    const { appearance, head, values, childObjects, items, type } = this.props
    const { isContentOpen } = this.state

    const Header = <HeaderWrapper
      appearance={appearance}
      isOpen={isContentOpen}
    >
      <HeaderContent>
        {head.icon && <HeadIcon content={head.iconLabel} src={head.icon} />}
        <HeaderTitle>{head.title}</HeaderTitle>
        {head.link && <Button
          appearance='subtle'
          href={head.link}
          target='_blank'
          spacing='none'
          iconBefore={<ShortcutIcon label='Link' size='small' />}
        />}
      </HeaderContent>
      <HeaderContent>
        {appearance !== 'nested' && !!head.actions.length &&
          <HeaderActionsWrapper>
            <DropdownMenu
              trigger={<div className='actions-btn'><MoreIcon label='Actions' size='small' /></div>}
              shouldFlip={false}
              position='bottom right'
            >
              <DropdownItemGroup>
                {head.actions.map((action, i) => <DropdownItem
                  onClick={action.callback}
                  key={action.text + i}
                >{action.text}</DropdownItem>)}
              </DropdownItemGroup>
            </DropdownMenu>
          </HeaderActionsWrapper>
        }
        <Chevron onClick={this.toggleContent} isOpen={isContentOpen} />
      </HeaderContent>
    </HeaderWrapper>

    const Container = <ContentWrapper>
      {values && values.map(field => (
        <FieldWrapper key={field.label}>
          <FieldTitle>{field.label}</FieldTitle>
          <div>{field.value}</div>
        </FieldWrapper>
      ))}
      {childObjects}
    </ContentWrapper>

    return (
      <Wrapper appearance={appearance} type={type}>
        {Header}
        {isContentOpen && Container}
        {items && items.length && <RelatedsWrapper>{items}</RelatedsWrapper>}
      </Wrapper>
    )
  }
}
