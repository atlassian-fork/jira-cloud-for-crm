import React from 'react'
import { bool, func } from 'prop-types'
import styled from 'styled-components'

import ChevronRightIcon from '@atlaskit/icon/glyph/chevron-right'
import ChevronDownIcon from '@atlaskit/icon/glyph/chevron-down'

const ChevronWrapper = styled.div`
  width: 24px;
  height: 24px;
  cursor: pointer;
`

Chevron.propTypes = {
  isOpen: bool.isRequired,
  onClick: func
}

export default function Chevron ({ isOpen, onClick = () => null }) {
  const ChevronRight = <ChevronRightIcon label='Open'>Open</ChevronRightIcon>
  const ChevronDown = <ChevronDownIcon label='Collapse'>Collapse</ChevronDownIcon>

  return (
    <ChevronWrapper onClick={onClick}>
      {isOpen ? ChevronDown : ChevronRight}
    </ChevronWrapper>
  )
}
