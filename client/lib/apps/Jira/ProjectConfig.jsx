import React from 'react'
import { observable, toJS } from 'mobx'
import { observer, Provider } from 'mobx-react'

import Page, { Grid, GridColumn } from '@atlaskit/page'
import Spinner from '@atlaskit/spinner'

import InitialPage from '../../components/InitialPage'
import MainPage from '../../components/MainPage'
import Addon from '../../services/api/Addon'
import { Flag, getContext } from '../../services/JiraAP'
import gas from '../../services/gas'
import LD from '../../services/LD'
import providers from '../../providers.json'

import logo from '../../../static/img/crmforjira.svg'

@observer
export default class App extends React.Component {
  @observable state = {
    account: {},
    currentOptions: [],
    providers,
    loading: true,
    processing: {
      connecting: false,
      saving: false,
      deleting: false
    },
    isManualSyncPanelVisible: false
  }

  connectAccount = async (provider, domain) => {
    const needPreAuth = provider === 'dynamics'
    const url = await Addon.account.getCreateUrl({ provider, needPreAuth })

    const screenX = parseInt(window.screenX)
    const screenY = parseInt(window.screenY)
    const windowWidth = parseInt(window.outerWidth)
    const windowHeight = parseInt(window.outerHeight)
    const popupWidth = provider === 'hubspot' ? 1175 : 450
    const popupHeight = 600
    const popupLeft = Math.round((windowWidth - popupWidth) / 2 + screenX)
    const popupTop = Math.round((windowHeight - popupHeight) / 3 + screenY)

    const popupParams = {
      width: popupWidth,
      height: popupHeight,
      left: popupLeft,
      top: popupTop,
      toolbar: 0,
      menubar: 0,
      location: 0,
      status: 0,
      scrollbars: 1,
      resizable: 0,
      chrome: 'yes'
    }

    const popupParamsStr = Object.keys(popupParams)
    .map(key => `${key}=${popupParams[key]}`)
    .join(',')

    window.manualConnect = (id, error) => this.storageHandler(id, true, error, domain)
    window.localStorage.removeItem('accountId')
    window.addEventListener('storage', e => e.key === 'accountId' && this.storageHandler(e.newValue))

    window.open(domain ? `${url}&domain=${domain}` : url, 'Authenticate', popupParamsStr)
  }

  storageHandler = async (accountId, manual = false, error, domain) => {
    if (!manual) {
      error = JSON.parse(window.localStorage.getItem('error'))
      window.localStorage.removeItem('error')
    }

    const hasErrors = this.handleConnectionError(manual, error)
    if (hasErrors) return

    this.state.processing.connecting = true

    if (domain) {
      window.localStorage.removeItem('accountId')
      window.removeEventListener('storage', this.storageHandler)

      await this.updateState()
      return
    }

    if (accountId) {
      const { jira: { project } } = await getContext()

      const params = {
        accountId,
        projectId: project.id
      }

      await Addon.connection.create(params)

      window.localStorage.removeItem('accountId')
      window.removeEventListener('storage', this.storageHandler)

      await this.updateState()

      gas.send({
        page: 'config-page',
        name: `${this.state.account.provider.id}.connect`
      })
    }
  }

  handleConnectionError (manual, error) {
    if (!error) return false

    const apiDisabled = ['API_CURRENTLY_DISABLED', 'API_DISABLED_FOR_ORG'].includes(error.code)
    const unauthorized = error.code.includes('UNAUTHORIZED')
    const sandboxOrgAuth = (
      error.code === 'invalid_grant' &&
      error.message === 'authentication failure' &&
      error.provider === 'salesforce'
    )

    if (apiDisabled) {
      const apiDisabledMessage = 'The REST API is not enabled for this User or Organization. This integration will not work unless the the REST API is enabled. Please contact your CRM administrator.'
      Flag.apiWarning({ body: apiDisabledMessage })
    } else if (unauthorized) {
      Flag.warning({
        title: 'Authorization failed',
        body: error.message
      })
    } else if (sandboxOrgAuth) {
      Flag.warning({
        body: 'It seems like you are trying to connect a sandbox organization. We don\'t support it. Please connect the production one.'
      })
    } else {
      const message = error.message || 'Something went wrong. Please try again later or contact support.'

      Flag.error({
        title: 'Authorization error',
        body: `${message} Error ID: ${error.id}.`
      })
    }

    window.removeEventListener('storage', this.storageHandler)

    return true
  }

  updateCurrentOptions (schema) {
    this.state.currentOptions = toJS(schema)
  }

  saveOptions = async () => {
    this.state.processing.saving = true
    const newOptions = toJS(this.state.currentOptions)

    const emptyObjects = newOptions.reduce((objects, group) => {
      if (!group.fields.length && !group.objects.length) {
        return [...objects, group.label]
      }

      return objects
    }, [])

    if (emptyObjects.length) {
      Flag.warning({
        title: 'You can\'t save configuration',
        body: `At least one field or object should be selected for ${emptyObjects.join(', ')}`
      })

      this.state.processing.saving = false
      return
    }

    const connectionId = this.state.account.connection.id

    const params = {
      id: connectionId,
      options: { schema: newOptions }
    }

    const saved = await Addon.connection.update(params)
    if (saved.id) {
      const connection = this.state.account.connection
      connection.options.schema = newOptions

      Flag.success({
        title: `${this.state.account.provider.name} configuration`,
        body: 'Successfully updated'
      })
    }

    this.state.processing.saving = false
  }

  cancelOptions = () => {
    const schema = this.state.account.connection.options.schema
    this.updateCurrentOptions(schema)
  }

  deleteConnection = async () => {
    this.state.processing.deleting = true
    const connectionId = this.state.account.connection.id
    const response = await Addon.connection.delete(connectionId)

    if (response) {
      Flag.success({
        title: `${this.state.account.provider.name}`,
        body: 'Successfully disconnected'
      })

      gas.send({
        page: 'config-page',
        name: `${this.state.account.provider.id}.disconnect`
      })

      this.updateState()

      return
    }

    this.state.processing.deleting = false
  }

  async updateState () {
    const { jira: { project } } = await getContext()
    const account = await Addon.account.show(project.id)

    if (account) {
      account.provider = providers.find(provider => provider.id === account.type)
      this.state.account = Object.assign({}, account)

      const schema = account.connection.options.schema
      this.updateCurrentOptions(schema)
    } else {
      this.state.account = {}
      this.updateCurrentOptions([])
    }

    this.state.loading = false
    this.state.processing = {
      connecting: false,
      saving: false,
      deleting: false
    }
  }

  async initFeatureFlags () {
    const client = await LD()

    client.on('ready', () => {
      // Enable CRMs which under development yet
      // Example:
      // Object.entries({
      //   providerName: client.variation('providerName')
      // }).forEach(([crmId, value]) => {
      //   const provider = this.state.providers.find(crm => crm.id === crmId)

      //   if (provider) provider.disabled = !value
      // })

      // Show manual sync panel
      this.state.isManualSyncPanelVisible = client.variation('manual-sync')
    })
  }

  async componentDidMount () {
    await this.initFeatureFlags()
    this.updateState()

    gas.send({
      page: 'config-page',
      name: 'visit'
    })
  }

  render () {
    const hasInfoToShow = Object.keys(this.state.account).length

    let CurrentPage
    if (this.state.loading) {
      CurrentPage = (
        <div className='preloader'>
          <Spinner size='large' />
        </div>
      )
    } else if (hasInfoToShow) {
      CurrentPage = (
        <Provider state={this.state}>
          <MainPage
            account={this.state.account}
            connectAccount={this.connectAccount}
            currentOptions={toJS(this.state.currentOptions)}
            saveOptions={this.saveOptions}
            cancelOptions={this.cancelOptions}
            deleteConnection={this.deleteConnection}
            processing={this.state.processing}
            isManualSyncPanelVisible={this.state.isManualSyncPanelVisible}
          />
        </Provider>
      )
    } else {
      CurrentPage = (
        <InitialPage
          providers={this.state.providers}
          connectAccount={this.connectAccount}
          connecting={this.state.processing.connecting}
        />
      )
    }

    return (
      <div className='project-config'>
        <Page>
          <Grid>
            <GridColumn>
              <h2 className='heading'><img src={logo} />CRM integration</h2>
              {CurrentPage}
            </GridColumn>
          </Grid>
        </Page>
      </div>
    )
  }
}
