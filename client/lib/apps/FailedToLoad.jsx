import React from 'react'
import SectionMessage from '@atlaskit/section-message'

export default function FailedToLoad () {
  return (
    <SectionMessage appearance='warning'>
      Something went wrong while loading the add-on. Please reload this page.
    </SectionMessage>
  )
}
