import memoize from 'lodash/memoize'
import { getProject } from './api/Jira'
import addon from '../../package.json'

async function showInfoMessage ({ title, body, close = 'manual' }) {
  window.AP.flag.create({ type: 'info', title, body, close })
}

async function showSuccessMessage ({ title, body, close = 'auto' }) {
  window.AP.flag.create({ type: 'success', title, body, close })
}

async function showWarningMessage ({ title = 'Warning!', body, close = 'manual' }) {
  window.AP.flag.create({ type: 'warning', title, body, close })
}

async function showAPIWarningMessage ({ title = 'Warning!', body, close = 'manual' }) {
  window.AP.flag.create({
    type: 'warning',
    title,
    body,
    close,
    actions: {
      link: 'Read more'
    }
  })

  window.AP.events.on('flag.action', () => {
    window.open(
      'https://help.salesforce.com/articleView?id=000005140&language=en_US&type=1',
      '_blank'
    )
  })
}

async function showErrorMessage ({ title = 'Something went wrong', body, close = 'manual' }) {
  window.AP.flag.create({ type: 'error', title, body, close })
}

async function showMatch ({ projectId, issueId, provider, callback }) {
  window.AP.dialog.create({
    key: 'find-customer-dialog',
    width: '500px',
    height: '365px',
    chrome: true,
    header: 'Link customer',
    submitText: 'Link',
    customData: {
      projectId,
      issueId,
      provider
    }
  })

  window.AP.events.once('dialog.close', data => data && callback(data))
}

async function showFeedback ({ type, page }) {
  window.AP.dialog.create({
    key: 'feedback-dialog',
    width: '600px',
    height: '275px',
    chrome: true,
    submitText: 'Submit',
    customData: { type, page }
  })

  window.AP.events.once('dialog.close', success => success &&
    showSuccessMessage({
      title: 'Thank you',
      body: 'Your feedback has been successfully submitted!'
    })
  )
}

async function hideFeedback () {
  window.AP.dialog.close()
}

async function disableSubmitButton () {
  window.AP.dialog.getButton('submit').disable()
}

async function enableSubmitButton () {
  window.AP.dialog.getButton('submit').enable()
}

async function toggleSubmitButton () {
  window.AP.dialog.getButton('submit').enable()
}

export async function getProjectConfigUrl ({ id, key, simplified } = {}) {
  const [url, { jira }] = await Promise.all([
    getLocation(),
    getContext()
  ])

  const { hostname } = new window.URL(url)
  const projectId = id || jira.project.id
  const projectKey = key || jira.project.key

  const isSimplified = id ? simplified : (await getProject(projectKey)).simplified

  return isSimplified
    ? `https://${hostname}/projects/${projectKey}/settings/apps/${addon.name}__crm-config-page`
    : `https://${hostname}/plugins/servlet/ac/${addon.name}/crm-config-page?project.key=${projectKey}&project.id=${projectId}`
}

export const getContext = memoize(() => {
  return new Promise(resolve => window.AP.context.getContext(resolve))
})

export const getAddonToken = () => {
  return new Promise(resolve => window.AP.context.getToken(resolve))
}

export const getLocation = memoize(() => {
  return new Promise(resolve => window.AP.getLocation(resolve))
})

export const getUserInfo = memoize(() => {
  return new Promise(resolve => window.AP.getCurrentUser(resolve))
})

export const Flag = {
  info: showInfoMessage,
  success: showSuccessMessage,
  warning: showWarningMessage,
  error: showErrorMessage,
  apiWarning: showAPIWarningMessage
}

export const Dialog = {
  match: showMatch,
  feedback: {
    show: showFeedback,
    hide: hideFeedback
  },
  submitButton: {
    disable: disableSubmitButton,
    enable: enableSubmitButton,
    toggle: toggleSubmitButton
  }
}

export const User = {
  info: getUserInfo
}
