// TODO: Switch to Flag
import showErrorMessage from '../JiraAP'

async function request (url) {
  return new Promise((resolve, reject) => {
    window.AP.request({
      url: url,
      success: (responseText) => {
        var data = JSON.parse(responseText)
        resolve(data)
      }
    })
  })
}

const catchMessage = (error, message) => showErrorMessage(`${message}. Please reload the page.`, error)

async function requestToJira (url, errorMessage) {
  try {
    return await request(url)
  } catch (error) {
    catchMessage(error, errorMessage)
  }
}

export const getProjects = async () => requestToJira(
  '/rest/api/2/project',
  'while getting projects list'
)
export const getProject = async (projectKey) => requestToJira(
  `/rest/api/2/project/${projectKey}`,
  'while getting project info'
)
