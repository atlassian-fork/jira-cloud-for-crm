import util from '../util'
import { Flag, getAddonToken } from '../JiraAP'

const jsonContentType = { 'Content-Type': 'application/json' }
let isBroken = false

const errorsQueue = []

const catchError = (error = {}, action) => {
  // do not show error duplicates
  if (errorsQueue.includes(action) || (errorsQueue.length && !action)) return

  // hold error action in queue for 1 sec
  errorsQueue.push(action)
  setTimeout(() => {
    const index = errorsQueue.findIndex(errorAction => errorAction === action)
    errorsQueue.splice(index, 1)
  }, 1000)

  if (error.code === 'AUTHENTICATION_FAILED') {
    return Flag.warning({
      title: 'Session expired',
      body: 'Please reload this page'
    })
  }

  return catchUnknownError(error, action)
}

const catchUnknownError = (error = {}, action) => {
  isBroken = true

  const dictionary = {
    salesforce: 'Salesforce',
    dynamics: 'Microsoft Dynamics',
    hubspot: 'HubSpot'
  }

  const title = error.source
    ? `${dictionary[error.source] || error.source} error`
    : 'Jira CRM integration add-on error'

  const body = [`Something went wrong${action ? ` while ${action}` : ''}.`]

  if (error.message && error.code !== 'UNEXPECTED_ERROR') {
    body.push(error.message)
  }

  body.push('Please try again later or contact support.')

  if (error.reqId) {
    body.push(`Error ID: ${error.reqId}.`)
  }

  console.error(error)

  return Flag.error({
    title,
    body: body.join(' ')
  })
}

async function request (url, params = {}) {
  if (isBroken) return catchError()

  const headers = params.headers || jsonContentType
  const body = params.body && JSON.stringify(params.body)

  headers.Authorization = await getAddonToken()

  return util.fetch(url, { ...params, headers, body })
}

async function getAccountCreateUrl ({ provider, needPreAuth = false, addonToken }) {
  if (!addonToken) {
    addonToken = await getAddonToken()
  }

  if (needPreAuth) {
    return `https://${window.location.hostname}/${provider}_pre_auth?addon-token=${addonToken}`
  }

  return `https://${window.location.hostname}/api/accounts/create/${provider}?addon-token=${addonToken}`
}

function showAccount (projectId) {
  return request(`/api/accounts?projectId=${projectId}`)
  .catch(error => catchError(error, 'getting account info'))
}

function createConnection (params) {
  return request(`/api/connections`, {
    method: 'POST',
    body: params
  })
  .catch(error => catchError(error, 'creating connection'))
}

function updateConnection (params) {
  return request(`/api/connections/${params.id}`, {
    method: 'PUT',
    body: params
  })
  .catch(error => catchError(error, 'updating connection'))
}

function deleteConnection (id) {
  return request(`/api/connections/${id}`, {
    method: 'DELETE'
  })
  .catch(error => catchError(error, 'deleting connection'))
}

function syncConnection (id) {
  return request(`/api/connections/${id}/sync`, {
    method: 'POST'
  })
  .catch(error => catchError(error, 'synchronizing connection'))
}

function listCustomers ({ projectId, issueId, search }) {
  return request(`/api/customers?projectId=${projectId}&issueId=${issueId}&search=${search}`)
}

function showCustomers (issueId, projectId, recordId, recordType, childRecordType) {
  const params = new window.URLSearchParams()
  const items = { recordId, recordType, childRecordType }

  Object.entries(items)
  .filter(([key, value]) => value)
  .forEach(([key, value]) => params.set(key, value))

  return request(`/api/customers/projects/${projectId}/issues/${issueId}?${params.toString()}`)
}

function createMatch (params) {
  return request(`/api/matches`, {
    method: 'POST',
    body: params
  })
  .catch(error => catchError(error, 'creating match'))
}

function deleteMatch (params) {
  return request(`/api/matches`, {
    method: 'DELETE',
    body: params
  })
  .catch(error => catchError(error, 'deleting match'))
}

export default {
  account: {
    getCreateUrl: getAccountCreateUrl,
    show: showAccount
  },
  connection: {
    create: createConnection,
    update: updateConnection,
    delete: deleteConnection,
    sync: syncConnection
  },
  customers: {
    show: showCustomers,
    list: listCustomers
  },
  match: {
    create: createMatch,
    delete: deleteMatch
  }
}
